var keyName;
const button = document.getElementById('restart');
const touchPad = document.getElementById('touchPad');
var sound_move = new Audio('_audio/audio_move.wav');
var sound_congrats = new Audio('_audio/audio_congrats.flac');
var sound_hit = new Audio('_audio/audio_hit_wall.wav');
var sound_gameover = new Audio('_audio/audio_gameover.wav');



// função que cria o display do labirinto ///////////////////
function loadGameDisplay() {
    const map = [
        "WWWWWWWWWWWWWWWWWWWWW",
        "W   W     W     W W W",
        "W W W WWW WWWWW W W W",
        "W W W   W     W W   W",
        "W WWWWWWW W WWW W W W",
        "W         W     W W W",
        "W WWW WWWWW WWWWW W W",
        "W W   W   W W     W W",
        "W WWWWW W W W WWW W F",
        "S     W W W W W W WWW",
        "WWWWW W W W W W W W W",
        "W     W W W   W W W W",
        "W WWWWWWW WWWWW W W W",
        "W       W       W   W",
        "WWWWWWWWWWWWWWWWWWWWW",
    ];
    
    createCells(map);
}



// função que cria cada Cell div do display ////////////////////
function createCells(map) {
    const display = document.getElementById('gameDisplay');
    for (let i = 0; i < 15; i++){
        for (let j = 0; j < 21; j++){
            if (map[i][j] === 'W') {
                let cell = document.createElement('div');
                cell.className = "cell wall";
                display.appendChild(cell);
            } else if (map[i][j] === 'S') {
                let cell = document.createElement('div');
                cell.className = "cell road";
                cell.id = "startCell";
                display.appendChild(cell);
            } else if (map[i][j] === 'F') {
                let cell = document.createElement('div');
                cell.className = "cell road";
                cell.id = "finalCell";
                display.appendChild(cell);
            } else {
                let cell = document.createElement('div');
                cell.className = "cell road";
                display.appendChild(cell);
            }
        }
    }
}



// função que posiciona o user na startCell ///////////////////////
function startPosition() {
    const startCell = document.getElementById('startCell');
    let user = document.createElement('div');
    user.className = "cell slideRight";
    user.id = "user";
    startCell.appendChild(user);

    window.currentCell = document.getElementById('user');
    window.positionCell = 189;
}



// função que crias as divs de vida ////////////////////////////////////
function boardLives() {
    const lives = document.getElementById('vidas');
    for (let i = 0; i < 3; i++) {
        let heart = document.createElement('div');
        heart.className = "vidas";
        lives.appendChild(heart);
    }
}



// função que implementa o movimento do user no labirinto ///////////////
function playGame(event) { 
    
    countMoves += 1;
    const counter = document.getElementById('moves');
    counter.innerHTML = countMoves;
    
    // const keyName = event.key;
    const display = document.getElementById('gameDisplay');
    const parent = currentCell.parentNode;


    let index = display.childNodes.length;
    let cells = display.childNodes;
       
    
    if (keyName === 'ArrowRight') {
        let nextCell = parent.nextSibling;
        if (nextCell.classList.contains('wall')) {
            lost();
        } else if (nextCell.id === "finalCell"){
            nextCell.appendChild(currentCell);
            sound_move.play();
            positionCell += 1;
            notification('Parabéns! Você encontrou a saída!');
            setTimeout(() => { sound_congrats.play() }, 100); 
            stopTimer();
            document.removeEventListener('keydown', keyPressed);
            touchPad.removeEventListener('touchstart', buttonTouched);
        } else {
            nextCell.appendChild(currentCell);
            sound_move.play();
            positionCell += 1;
        }
    }



    if (keyName === 'ArrowLeft') {
        let cellBefore = parent.previousSibling;
        if (cellBefore.classList.contains('wall') | parent.id === "startCell") {
            lost();
        } else {
            cellBefore.appendChild(currentCell);
            sound_move.play();
            positionCell -= 1;
        }
    }



    if (keyName === 'ArrowUp') {
        let cellUpIndex = positionCell - 21;
        let cellUp = cells[cellUpIndex];
        if (cellUp.classList.contains('wall')) {
            lost();
        } else {
            cellUp.appendChild(currentCell);
            sound_move.play();
            positionCell = positionCell-21;
        }
    }


    if (keyName === 'ArrowDown') {
        let cellDownIndex = positionCell + 21;
        let cellDown = cells[cellDownIndex];
        if (cellDown.classList.contains('wall')) {
            lost();
        } else {
            cellDown.appendChild(currentCell);
            sound_move.play();
            positionCell = positionCell + 21;
        }
    }
}



// função que implementa as vidas e o Game Over
function lost() {
    const lives = document.getElementById('vidas');
    let countLives = lives.childNodes.length;
    if (countLives === 1) {
        lives.removeChild(lives.lastElementChild);
        document.removeEventListener('keydown', keyPressed);
        touchPad.removeEventListener('touchstart', buttonTouched);
        stopTimer();
        notification('GAME OVER!');
        sound_gameover.play();
    } else {
        lives.removeChild(lives.lastElementChild);
        notification('Você bateu na parede. Perdeu uma vida!');
        sound_hit.play();
    }
}



// função que imprime mensagem na tela
function notification(message) {
    const lives = document.getElementById('vidas');
    let countLives = lives.childNodes.length;
    const msg = document.getElementById('msg');
    msg.innerHTML = message;
    if (positionCell != 188 && countLives != 0) { // finalCell e zero vidas -> mensagem não some
        setTimeout(function() {msg.innerHTML = "";}, 1500);
    } else {
        clearTimeout;
    }
}


// função que implementa a contagem de tempo
function tempo() {
    let seconds = 0;
    let minutes = 0;

    window.timer = setInterval(function () {
        if (seconds == 59) {
            minutes++;
            seconds = 0;
        } else {
            seconds++;
        }
        let timerID = document.getElementById('timer');
        timerID.textContent = (String(minutes).padStart(2, '0')) + ':' + (String(seconds).padStart(2, '0'));

    },  1000);
}

function stopTimer() {
    clearInterval(timer);
}


// função que reinicia o jogo
function restartGame() {
    stopTimer();

    touchPad.addEventListener('touchstart', tempo, {once: true});
    document.addEventListener('keydown', tempo, {once: true})

    const timer = document.getElementById('timer');
    timer.innerHTML = "00:00";

    countMoves = 0;
    const counter = document.getElementById('moves');
    counter.innerHTML = countMoves;

    const parent = currentCell.parentNode;
    parent.removeChild(currentCell);

    const msg = document.getElementById('msg');
    msg.innerHTML = "";

    const lives = document.getElementById('vidas');
    let countLives = lives.childNodes.length;
    for (let i = countLives - 1; i >= 0; i--) {
        lives.removeChild(lives.childNodes[i]);
    }
   
    startPosition();
    boardLives();

    document.addEventListener('keydown', keyPressed);
    touchPad.addEventListener('touchstart', buttonTouched, false);

    button.addEventListener('click',restartGame);
    button.addEventListener('touchstart', restartGame);
}

function keyPressed (event) {
    keyName = event.key;
    playGame(event);
}

function buttonTouched (event) {
    var touchlist = event.touches;
    for (var i=0; i<touchlist.length; i++){ 
        console.log(`event.touches: ${touchlist[i].target.id}`);

        keyName = touchlist[i].target.id;

        var btnClicked = document.getElementById(touchlist[i].target.id);
        btnClicked.style.backgroundColor = "#f58f5363";
        setTimeout(() => {btnClicked.style.backgroundColor = "#ffffff56";},200)
    }
    playGame(event);
}

// FUNÇÃO PRINCIPAL ///////////////////////////////////////////
window.onload = main;
function main() {

    window.countMoves = 0;
    loadGameDisplay();
    startPosition();

    document.addEventListener('keydown', keyPressed)
    document.addEventListener('keydown', tempo, {once: true});

    
    touchPad.addEventListener('touchstart', buttonTouched, false);
    touchPad.addEventListener('touchstart', tempo, {once: true});

    
    button.addEventListener('click',restartGame);
    button.addEventListener('touchstart',restartGame);
}